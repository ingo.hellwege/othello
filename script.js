/* init */
var plr=true;           // true:p1(b),false:p2(w)
var ovr=false;          // game over status
var grid=new Array();   // the grid
var rows=12;            // rows
var cols=22;            // cols
var dbug=false;         // debug mode
var nln='\n';           // line break
var markerWhite='O';    // board marker
var markerBlack='X';    // board marker
var markerValid='+';    // board marker
var markerEmpty=' ';    // board marker
var cmp=true;           // computer opponent?
var lvl=1;              // cleverness (0:random,1:avg turns,2:max turns)
var dng=true;           // allow diagonal calculations
var btnaud='';          // button audio
var clkaud='';          // color button audio
var erraud='';          // error audio



/* add class to element */
function addClassToElementById(elmid,elmcls){
  var cls=getClassStringForElementById(elmid);
  if(cls.includes(elmcls)==false){
    var newcls=cls.concat(' '+elmcls);
    document.getElementById(elmid).className=newcls;
  }
}

/* remove class from element */
function removeClassFromElementById(elmid,elmcls){
  var cls=getClassStringForElementById(elmid);
  var newcls=cls.split(elmcls).join('');
  document.getElementById(elmid).className=newcls;
}

/* generate box id for click */
function getBoxIdForRowAndCol(r,c){
  return 'box-'+r+'-'+c;
}

/* get the string with class names for element */
function getClassStringForElementById(elmid){
  return document.getElementById(elmid).className;
}

/* do click on element by id */
function clickOnElementById(elmid){
  document.getElementById(elmid).click();
}

/* hide cover layer */
function hideCover(){
  addClassToElementById('cover1','cover1hide');
  addClassToElementById('cover2','cover2hide');
}

/* center game */
function centerWrapper(){
  var left=Math.floor((document.documentElement.clientWidth-document.getElementById('wrapper').offsetWidth)/2);
  document.getElementById('wrapper').style.marginLeft=left+'px';
  var top=Math.floor((document.documentElement.clientHeight-document.getElementById('wrapper').offsetHeight)/2);
  document.getElementById('wrapper').style.marginTop=top+'px';
  var btn=document.getElementById('new').offsetWidth;
  var left=Math.floor((document.getElementById('wrapper').offsetWidth-(btn*3))/2)-70;
  document.getElementById('info').style.marginLeft=left+'px';
}



/* get start and end position in string for regex */
function getPositionForRegEx(str,regex,dir='>') {
  // setup
  dir=dir.toLowerCase();
  var pos=-1;
  var rex=new RegExp(regex);
  var match=rex.exec(str);
  // do we have a match and wht to do with it
  if(match!==null){
    switch(dir){
      case 'x':
        pos=match.index+1;
        break;
      case '<':
        pos=(match.index+match[0].length)-1;
        break;
      case '>':
      default:
        pos=match.index;
        break;
    }
  }
  // return
  return pos;
}

/* get amount of turns for regex */
function getAmountOfTurnsForMove(move) {
  // setup
  var amt=0;
  var crd=move.split(',');
  var r=parseInt(crd[0]);
  var c=parseInt(crd[1]);
  myColorMarker=getMarkerForPlayer(plr);
  opColorMarker=getMarkerForPlayer(!plr);
  var regExLR='[+]{1}['+opColorMarker+']+['+myColorMarker+']{1}';
  var regExRL='['+myColorMarker+']{1}['+opColorMarker+']+[+]{1}';
  // horizontal
  var line='';for(cc=0;cc<cols;cc++){line+=grid[r][cc];}
  if(getPositionForRegEx(line,regExLR,'>')>=0){
    var p=1;while(parseInt(c+p)<cols&&grid[r][parseInt(c+p)]==opColorMarker){amt++;p++;}
  }
  if(getPositionForRegEx(line,regExRL,'<')>=0){
    var p=-1;while(parseInt(c+p)>=0&&grid[r][parseInt(c+p)]==opColorMarker){amt++;p--;}
  }
  // vertical
  var line='';for(rr=0;rr<rows;rr++){line+=grid[rr][c];}
  if(getPositionForRegEx(line,regExLR,'>')>=0){
    var p=1;while(parseInt(r+p)<rows&&grid[parseInt(r+p)][c]==opColorMarker){amt++;p++;}
  }
  if(getPositionForRegEx(line,regExRL,'<')>=0){
    var p=-1;while(parseInt(r+p)>=0&&grid[parseInt(r+p)][c]==opColorMarker){amt++;p--;}
  }
  // diagonal r-c-\
  if(dng==true){
    var p=0;var line='';while(parseInt(c+p)<cols&&parseInt(r+p)<rows){line+=grid[parseInt(r+p)][parseInt(c+p)];p++;}
    if(getPositionForRegEx(line,regExLR,'>')>=0){
      var p=1;while(parseInt(r+p)<rows&&parseInt(c+p)<cols&&grid[parseInt(r+p)][parseInt(c+p)]==opColorMarker){amt++;p++;}
    }
    if(getPositionForRegEx(line,regExLR,'<')>=0){
      var p=-1;while(parseInt(r+p)>=0&&parseInt(c+p)>=0&&grid[parseInt(r+p)][parseInt(c+p)]==opColorMarker){amt++;p--;}
    }
  }
  // diagonal r-c-/
  if(dng==true){
    var p=0;var line='';while(parseInt(c+p)<cols&&parseInt(r-p)>=0){line+=grid[parseInt(r-p)][parseInt(c+p)];p++;}
    if(getPositionForRegEx(line,regExLR,'>')>=0){
      var p=1;while(parseInt(r+p)>=0&&parseInt(c+p)<cols&&grid[parseInt(r-p)][parseInt(c+p)]==opColorMarker){amt++;p++;}
    }
    if(getPositionForRegEx(line,regExLR,'<')>=0){
      var p=-1;while(parseInt(r-p)<rows&&parseInt(c+p)>=0&&grid[parseInt(r-p)][parseInt(c+p)]==opColorMarker){amt++;p--;}
    }
  }
  // return
  return amt;
}

/* get marker for player */
function getMarkerForPlayer(plrflg){
  if(plrflg){return markerBlack;}
  return markerWhite;
}

/* get color name for player */
function getColorNameForPlayer(plrflg){
  if(plrflg){return 'black';}
  return 'white';
}

/* toggle player */
function togglePlayer(){
  // remove all possible colors
  removeClassFromElementById('plr','black');
  removeClassFromElementById('plr','white');
  removeClassFromElementById('plr','ovr');
  // switch and set color for player
  plr=!plr;
  addClassToElementById('plr',getColorNameForPlayer(plr));
}

/* generate dynamic game grid */
function generateGrid(){
  console.log('generate grid');
  for(r=0;r<rows;r++){
    grid[r]=new Array();
    for(c=0;c<cols;c++){
      grid[r][c]=markerEmpty;
    }
  }
}

/* refresh grid */
function refreshGrid(){
  console.log('refresh grid');
  setGridHtml(generateGridHtml());
  generateHints();
}

/* set first stones on grid */
function setFirstStones(){
  console.log('set first stones');
  midX=Math.floor(rows/2)-1;
  midY=Math.floor(cols/2)-1;
  grid[midX][midY]=markerWhite;
  grid[midX][midY+1]=markerBlack;
  grid[midX+1][midY]=markerBlack;
  grid[midX+1][midY+1]=markerWhite;
}

/* set hints for possible moves */
function generateHints(){
  console.log('generate hints');
  // setup
  removeHints();
  myColorMarker=getMarkerForPlayer(plr);
  opColorMarker=getMarkerForPlayer(!plr);
  var regExLR='['+markerEmpty+']{1}['+opColorMarker+']+['+myColorMarker+']{1}';
  var regExRL='['+myColorMarker+']{1}['+opColorMarker+']+['+markerEmpty+']{1}';
  // horizontal
  var pos=0;
  for(r=0;r<rows;r++){
    var line='';for(c=0;c<cols;c++){line+=grid[r][c];}
    pos=getPositionForRegEx(line,regExLR,'>');
    if(pos>=0){setMarkerValidForRowAndCol(r,pos);}
    pos=getPositionForRegEx(line,regExRL,'<');
    if(pos>=0){setMarkerValidForRowAndCol(r,pos);}
  }
  // vertical
  var pos=0;
  for (c=0;c<cols;c++){
    var line='';for(r=0;r<rows;r++){line+=grid[r][c];}
    pos=getPositionForRegEx(line,regExLR,'>');
    if(pos>=0){setMarkerValidForRowAndCol(pos,c);}
    pos=getPositionForRegEx(line,regExRL,'<');
    if(pos>=0){setMarkerValidForRowAndCol(pos,c);}
  }
  // diagonal left-right-\
  if(dng==true){
    var pr=0;
    var pc=cols-1;
    while(pr<rows){
      var p=0;var line='';while(parseInt(pc+p)<cols&&parseInt(pr+p)<rows){line+=grid[parseInt(pr+p)][parseInt(pc+p)];p++;}
      pos=getPositionForRegEx(line,regExLR,'>');
      if(pos>=0){setMarkerValidForRowAndCol(parseInt(pr+pos),parseInt(pc+pos));}
      pos=getPositionForRegEx(line,regExRL,'<');
      if(pos>=0){setMarkerValidForRowAndCol(parseInt(pr+pos),parseInt(pc+pos));}
      // count
      if(pc>-1){pc--;}
      if(pc==-1){pc=0;pr++;}
    }
  }
  // diagonal top-down-/
  if(dng==true){
    var pr=0;
    var pc=0;
    while(pc<cols){
      var p=0;var line='';while(parseInt(pc+p)<cols&&parseInt(pr-p)>=0){line+=grid[parseInt(pr-p)][parseInt(pc+p)];p++;}
      pos=getPositionForRegEx(line,regExLR,'>');
      if(pos>=0){setMarkerValidForRowAndCol(parseInt(pr-pos),parseInt(pc+pos));}
      pos=getPositionForRegEx(line,regExRL,'<');
      if(pos>=0){setMarkerValidForRowAndCol(parseInt(pr-pos),parseInt(pc+pos));}
      // count
      if(pr<rows){pr++;}
      if(pr==rows){pr=rows-1;pc++;}
    }
  }
}

/* set marker for valid move */
function setMarkerValidForRowAndCol(r,c){
  // int them
  r=parseInt(r);
  c=parseInt(c);
  // set marker and class
  if(r<rows&&c<cols){
    if(grid[r][c]==markerEmpty){
      grid[r][c]=markerValid;
      var boxid=getBoxIdForRowAndCol(r,c);
      addClassToElementById(boxid,'valid');
    }
  }
}

/* remove marker for valid move */
function removeMarkerValidForRowAndCol(r,c){
  // int them
  r=parseInt(r);
  c=parseInt(c);
  // set marker and class
  if(r<rows&&c<cols){
    if(grid[r][c]==markerValid){
      grid[r][c]=markerEmpty;
      var boxid=getBoxIdForRowAndCol(r,c);
      removeClassFromElementById(boxid,'valid');
    }
  }
}

/* remove all hints */
function removeHints(){
  for(r=0;r<rows;r++){
    for(c=0;c<cols;c++){
      if(grid[r][c]==markerValid){removeMarkerValidForRowAndCol(r,c);}
    }
  }
}

/* get amount of possible moves */
function getPossibleMovesAmount(){
  var amt=document.querySelectorAll('div.valid').length;
  return amt;
}

/* generate grid html */
function generateGridHtml(){
  console.log('generate grid html');
  // setup
  var html='';
  // dynamic grid
  for(r=0;r<rows;r++){
    for(c=0;c<cols;c++){
      // show marker in debug mode
      var markstr='';
      if(dbug==true){markstr=grid[r][c];}
      // show colors for stones
      var colcls='';
      if(grid[r][c]==markerBlack){colcls+=' '+getColorNameForPlayer(true);}
      if(grid[r][c]==markerWhite){colcls+=' '+getColorNameForPlayer(false);}
      // set box
      var boxid=getBoxIdForRowAndCol(r,c);
      html+='        <div class="box'+colcls+'" id="'+boxid+'" attr-row="'+r+'" attr-col="'+c+'">'+markstr+'</div>'+nln;
    }
    // clear row
    html+='        <div class="clear"></div>'+nln;
  }
  // return
  return html;
}

/* set grid html */
function setGridHtml(html){
  console.log('set grid html');
  document.getElementById('boxes').innerHTML=html;
}

/* show hints for next move */
function toggleHints(){
  console.log('toggle hints');
  for(r=0;r<rows;r++){
    for(c=0;c<cols;c++){
      var boxid=getBoxIdForRowAndCol(r,c);
      var cls=getClassStringForElementById(boxid);
      if(grid[r][c]==markerValid&&cls.includes('hint')==false){
        addClassToElementById(boxid,'hint');
      }else{
        removeClassFromElementById(boxid,'hint');
      }
    }
  }
}

/* turning stones */
function turningStonesForClick(r,c){
  console.log('turning stones')
  // setup
  var r=parseInt(r);
  var c=parseInt(c);
  myColorMarker=getMarkerForPlayer(plr);
  opColorMarker=getMarkerForPlayer(!plr);
  var regExLR='[+]{1}['+opColorMarker+']+['+myColorMarker+']{1}';
  var regExRL='['+myColorMarker+']{1}['+opColorMarker+']+[+]{1}';
  // horizontal
  var line='';for(cc=0;cc<cols;cc++){line+=grid[r][cc];}
  if(getPositionForRegEx(line,regExLR,'>')>=0){
    var pos=c+1;
    while(pos<cols&&grid[r][pos]==opColorMarker){
      grid[r][pos]=myColorMarker;pos++;
    }
  }
  if(getPositionForRegEx(line,regExRL,'<')>=0){
    var pos=c-1;
    while(pos>=0&&grid[r][pos]==opColorMarker){
      grid[r][pos]=myColorMarker;pos--;
    }
  }
  // vertical
  var line='';for(rr=0;rr<rows;rr++){line+=grid[rr][c];}
  if(getPositionForRegEx(line,regExLR,'>')>=0){
    var pos=r+1;
    while(pos<rows&&grid[pos][c]==opColorMarker){
      grid[pos][c]=myColorMarker;pos++;
    }
  }
  if(getPositionForRegEx(line,regExRL,'<')>=0){
    var pos=r-1;
    while(pos>=0&&grid[pos][c]==opColorMarker){
      grid[pos][c]=myColorMarker;pos--;
    }
  }
  // diagonal r-c-\
  if(dng==true){
    var p=0;var line='';while(parseInt(c+p)<cols&&parseInt(r+p)<rows){line+=grid[parseInt(r+p)][parseInt(c+p)];p++;}
    if(getPositionForRegEx(line,regExLR,'>')>=0){
      var p=1;
      while(grid[parseInt(r+p)][parseInt(c+p)]==opColorMarker){
        grid[parseInt(r+p)][parseInt(c+p)]=myColorMarker;p++;
      }
    }
    var p=0;var line='';while(parseInt(c-p)>=0&&parseInt(r-p)>=0){line+=grid[parseInt(r-p)][parseInt(c-p)];p++;}
    if(getPositionForRegEx(line,regExLR,'<')>=0){
      var p=-1;
      while(grid[parseInt(r+p)][parseInt(c+p)]==opColorMarker){
        grid[parseInt(r+p)][parseInt(c+p)]=myColorMarker;p--;
      }
    }
  }
  // diagonal r-c-/
  if(dng==true){
    var p=0;var line='';while(parseInt(c+p)<cols&&parseInt(r-p)>=0){line+=grid[parseInt(r-p)][parseInt(c+p)];p++;}
    if(getPositionForRegEx(line,regExLR,'>')>=0){
      var p=1;
      while(grid[parseInt(r-p)][parseInt(c+p)]==opColorMarker){
        grid[parseInt(r-p)][parseInt(c+p)]=myColorMarker;p++;
      }
    }
    var p=0;var line='';while(parseInt(c-p)>=0&&parseInt(r+p)<rows){line+=grid[parseInt(r+p)][parseInt(c-p)];p++;}
    if(getPositionForRegEx(line,regExLR,'<')>=0){
      var p=-1;
      while(grid[parseInt(r-p)][parseInt(c+p)]==opColorMarker){
        grid[parseInt(r-p)][parseInt(c+p)]=myColorMarker;p--;
      }
    }
  }
  // set marker for move
  grid[r][c]=myColorMarker;
}

/* next player: check possible moves (0:toggle back) */
function nextPlayer(){
  togglePlayer();
  generateHints();
  if(getPossibleMovesAmount()==0){
    togglePlayer();
    generateHints();
  }
  if(dbug==true){refreshGrid()}
}

/* count stones for player */
function countStonesForPlayer(plrflg){
  var colName=getColorNameForPlayer(plrflg);
  var marker=getMarkerForPlayer(plrflg);
  var amt=document.querySelectorAll('div.box.'+colName).length;
  document.getElementById('res-'+colName).innerHTML=amt;
}

/* count stones for both players */
function countStones(){
  console.log('count stones')
  countStonesForPlayer(plr);
  countStonesForPlayer(!plr);
}

/* did player win or not */
function checkForEndGame(){
  console.log('check for end game');
  // res-b + res-w = rows * cols
  var resb=parseInt(document.getElementById('res-black').innerHTML);
  var resw=parseInt(document.getElementById('res-white').innerHTML);
  if(resb+resw==rows*cols){endflg=true;}
  // no possible moves for both players
  var plrend=0;
  var amt=0;
  for(p=0;p<2;p++){
    amt=getPossibleMovesAmount();
    if(amt==0){plrend++;}
    togglePlayer();
    generateHints();
  }
  // no possible moves for both players
  if(plrend==2){endGame();}
}



/* computer player is making a move */
function computerMove(){
  console.log('compuer move');
  // generate list of possible moves
  generateHints();
  if(dbug==true){refreshGrid();}
  // get possible moves
  var mvs=new Array();
  var elms=document.querySelectorAll('div.valid');
  elms.forEach((item)=>{mvs.push(parseInt(item.getAttribute('attr-row'))+','+item.getAttribute('attr-col'));});
  // setup
  var boxid='';
  // how clever should the computer react
  switch(lvl) {
    case 2:
      boxid=computerMoveMaximum(mvs);
      break;
    case 1:
      boxid=computerMoveAverage(mvs);
      break;
    case 0:
    default:
      boxid=computerMoveRandom(mvs);
  }
  // nothing found, do a random one
  if(boxid==''){boxid=computerMoveRandom();}
  // do click
  setTimeout(()=>{document.getElementById(boxid).click();},500);
}

/* calculate amount of turns for each move */
function calculateAmountForMoves(mvs){
  var cmvs=new Array();
  // get turn amount for every move
  mvs.forEach((move)=>{
    var amt=getAmountOfTurnsForMove(move);
    var arr={'amt':amt,'move':move};
    cmvs.push(arr);
  });
  // sort by amount asc
  cmvs.sort((a,b)=>{return a.amt-b.amt;});
  return cmvs;
}

/* choose the move with the maximum (last one) amount of turns */
function computerMoveMaximum(mvs){
  var cmvs=calculateAmountForMoves(mvs);
  var crd=cmvs.pop().move.split(',');
  var boxid=getBoxIdForRowAndCol(crd[0],crd[1]);
  return boxid;
}

/* choose a move with an average (middle one) amount of turns */
function computerMoveAverage(mvs){
  var cmvs=calculateAmountForMoves(mvs);
  var which=Math.floor(mvs.length/2);
  var crd=mvs[which].split(',');
  var boxid=getBoxIdForRowAndCol(crd[0],crd[1]);
  return boxid;
}

/* choose a random move */
function computerMoveRandom(mvs){
  // choose a move, show which one and perform a click
  var which=Math.floor(Math.random()*mvs.length);
  var crd=mvs[which].split(',');
  var boxid=getBoxIdForRowAndCol(crd[0],crd[1]);
  return boxid;
}



/* setup game audio files */
function setupAudio(){
  btnaud=new Audio('button.wav');
  btnaud.volume=0.15;
  clkaud=new Audio('click.wav');
  clkaud.volume=0.5;
  erraud=new Audio('error.wav');
  erraud.volume=0.5;
}



/* init game */
function init(){
  console.log('init');
  console.log('cmp: '+cmp);
  console.log('lvl: '+lvl);
  // fresh start
  setupAudio();
  grid=new Array();
  generateGrid();
  setFirstStones();
  refreshGrid();
  countStones();
  plr=false;
  nextPlayer();
  ovr=false;
  // and go
  console.log('ready!');
}

/* end of game */
function endGame(){
  console.log('game ended');
  erraud.play();
  ovr=true;
  // set color marker
  addClassToElementById('plr','ovr');
}



/* listener for key */
document.addEventListener('keydown',(event)=>{
  switch(event.which){
    // n(ew)
    case 78:
      if(ovr==true){clickOnElementById('new');}
      break;
    // h(elp)
    case 72:
      if(ovr==false){clickOnElementById('hnt');}
      break;
    // r(eset)
    case 82:
      init();
      break;
  }
},false);

/* listener for click */
document.addEventListener('click',(event)=>{
  // clicked on a button
  if(event.target.matches('.button')){
    btnaud.cloneNode(true).play();
    var action=event.target.getAttribute('attr-action');
    console.log('button: '+action);
    // what to do?
    if(ovr==true&&action=='new'){init();}
    if(ovr==false&&action=='hnt'){dbug=!dbug;refreshGrid();}
  }
  // clicked on a box
	if(ovr==false&&event.target.matches('.box')){
    clkaud.cloneNode(true).play();
    var r=event.target.getAttribute('attr-row');
    var c=event.target.getAttribute('attr-col');
    // turning stones, refresh grid, count stones, switch player
    if(grid[r][c]==markerValid){
      turningStonesForClick(r,c);
      refreshGrid();
      countStones();
      // check if game is over
      checkForEndGame();
      // toggle player
      if(ovr==false){nextPlayer();}
      // white (false) can be a computer player
      if(ovr==false&&cmp==true&&plr==false){computerMove();}
    }
  }
},false);



/* wait until document is fully loaded and ready to start */
let stateCheck=setInterval(()=>{
  if(document.readyState=='complete'){
    clearInterval(stateCheck);
    init();
    centerWrapper();
    hideCover();
  }
},250);
